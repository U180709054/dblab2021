call selectAllCustomers();

call getCustomersByCountry("Spain");
set @country ="UK";
call getCustomersByCountry(@country);
select @country;

select * from shippers;

select count(orderID)
from orders join shippers on orders.ShipperID = shippers.ShipperID
where ShipperName = "Speedy Express";


set @orderCount = 0;
call getNumberOfOrdersByShipper("Speedy Express", @orderCount);


set @beg = 100;
set @inc = 10;

call counter(@beg, @inc);
select @beg, @inc;
