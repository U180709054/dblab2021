select * from denormalized;
load data 
#infile 'C:\\Users\\Esmanur\\Desktop\\denormalized.cv'
infile 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\denormalized.csv'
into table  denormalized
columns terminated by ';';

insert into movies (movie_id, title, ranking, rating, year, votes, duration, oscars ,budget)
select distinctrow movie_id, title, ranking, rating, year, votes, duration, oscars ,budget
from denormalized;

select * from movies;

insert into countries(country_id, country_name)
select distinctrow producer_country_id, producer_country_name
from denormalized
union
select distinctrow director_country_id, director_country_name
from denormalized
union
select distinctrow star_country_id, star_country_name
from denormalized
order by producer_country_id;

select * from countries;
show variables like "secure_file_priv";